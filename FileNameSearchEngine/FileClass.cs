﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileNameSearchEngine
{
    public class FileClass
    {
        public string center = string.Empty;
        public string hospital = string.Empty;
        public string scrNo = string.Empty;
        public string partId = string.Empty;
        public string birthOrder = string.Empty;
        public string order = string.Empty;
        public string type = string.Empty;
        public string project = string.Empty;
        public int rowNo = -1; //for excel records
        public int typePos = -1; //for file records
        public int length = -1; //for file records
        public bool found = false;
        public bool processed = false;
        public bool error = false; //for file records
        public string errorMessage = string.Empty;
        public string fileName = string.Empty;
        public string fileExt = string.Empty;
    }
}
