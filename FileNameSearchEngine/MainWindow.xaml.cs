﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
//using System.IO;
using System.Windows;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Delimon.Win32.IO;

namespace FileNameSearchEngine
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string path = @"C:\My files\WHO_SIS\FileNameSearchEngine\";
        string fileFolder, dbFolder;
        char[] splitters = { '-', '_' };
        List<string> dirsList = new List<string>();
        List<string> types5 = new List<string>() { "SCR" };
        List<string> typesOrderRequired = new List<string>() { "SCR", "TRE", "ANA", "PPR", "AER", "SAE" };
        List<string> typesBirthRequired = new List<string>() { "BIR", "OBM", "PNA", "PNF7", "PNF28", "PNR", "PCD", "ESN", "VAF", "PVD" };
        List<string> typesOrderAndBirthOrderNotRequired = new List<string>() { "BAS", "DEL", "PPA", "PPF7", "PPF28", "ESW", "HBF" };
        List<string> allFormTypes = new List<string>() { "SCR", "TRE", "ANA", "PPR", "PNR", "AER", "SAE", "BIR", "OBM", "PNA", "PNF7", "PNF28", "PNR", "PCD", "ESN", "VAF", "PVD", "BAS", "DEL", "PPA", "PPF7", "PPF28", "ESW", "HBF" };
        List<string> allowedFileExtensions = new List<string>() { ".pdf", ".jpg", ".PDF", ".JPG" };

        const int startingRow = 4;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void fileFolderBtn_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                string path = @"C:\My files\WHO_SIS\FileNameSearchEngine\Files\";

                if (Directory.Exists(path))
                    dialog.SelectedPath = path;
                else
                    dialog.SelectedPath = @"C:\";
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    fileFolderPath.Text = dialog.SelectedPath;
                    fileFolder = fileFolderPath.Text;
                }
            }
        }


        /*Recursively get all folders and all-level subfolders that match pattern | Result => dirList*/
        private void GetAllFolders(string path, string pattern)
        {
            try
            {
                foreach (string folder in Directory.GetDirectories(path))
                {
                    if(Path.GetFileName(folder).ToLower().Contains(pattern.ToLower()) &&
                        (!Path.GetFileName(folder).ToLower().Contains("scrlog")) &&
                        (!Path.GetFileName(folder).ToLower().Contains("old medication"))
                      )
                        dirsList.Add(folder);
                    GetAllFolders(folder, pattern);
                }
            }
            catch (Exception ex)
            {
                // Get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                MessageBox.Show("Line#" + line + ";Error:" + ex.Message);
            }
        }

        private bool LogFoundDirs(string[] dirs, string type)
        {
            if (dirs.Length > 0)
            {
                WriteToLog("The following folder(s) will be processed for type " + type + ":");
                foreach (string dir in dirs)
                    WriteToLog(dir);
                return true;
            }
            else
            {
                WriteToLog("No directories found for this database file. Moving to next..");
                return false;
            }

        }


        private int FindColumnPosition(Excel.Worksheet sheet, string colName, int rowNo, int startingColNo, int endingColNo)
        {
            int pos = 0;
            for (int s = startingColNo; s <= endingColNo; s++)
            {
                if (GetCell(sheet, rowNo, s).ToLower().Trim() == colName.ToLower().Trim())
                {
                    pos = s;
                    break;
                }
                else
                    continue;
            }
            if (pos == 0)
                WriteToLog("Could not find the "+colName+" column in the worksheet. Aborted.");
            return pos;
        }

        private bool isFormatCorrect(Excel.Worksheet sheet, int headerRow, string type, ref int dropBoxColumnNo)
        {
            if ((GetCell(sheet, headerRow, 1).ToLower().Trim() != "center") ||
                    (GetCell(sheet, headerRow, 2).ToLower().Trim() != "hospital") ||
                    (GetCell(sheet, headerRow, 3).ToLower().Trim() != "scr number") ||
                    (GetCell(sheet, headerRow, 4).ToLower().Trim() != "participant"))
            {
                WriteToLog("Encountered incorrect format. Aborted.");
                return false;
            }
            else
            {
                /*Get the position of resulting column*/
                dropBoxColumnNo = FindColumnPosition(sheet, "dropbox", startingRow - 1, 1, 10);
                if (dropBoxColumnNo == 0)
                {  
                    WriteToLog("Encountered incorrect format(dropbox column not found). Aborted.");
                    return false;
                }
                if (!typesOrderAndBirthOrderNotRequired.Contains(type))
                {
                    if(dropBoxColumnNo<6)
                    //if (GetCell(sheet, headerRow, 5).ToLower().Trim() != "order")
                    {
                        WriteToLog("Encountered incorrect format(order column not found). Aborted.");
                        return false;
                    }
                }
            }
            return true;
        }

        private void GetExcelRows(Excel.Worksheet sheet, int startingRow, int dataRowsCount, string type, ref List<FileClass> sheetRecords, ref string center, ref string hospital)
        {
            center = GetCell(sheet, startingRow, 1).ToLower().Trim().PadLeft(4,'0');
            hospital = GetCell(sheet, startingRow, 2).ToLower().Trim();

            for (int k = startingRow; k <= dataRowsCount; k++)
            {
                if (string.IsNullOrEmpty(GetCell(sheet, k, 1).ToLower().Trim()))   break;
                FileClass f = new FileClass()
                {
                    center = GetCell(sheet, k, 1).ToLower().Trim().PadLeft(4,'0'),
                    hospital = GetCell(sheet, k, 2).ToLower().Trim(),
                    scrNo = GetCell(sheet, k, 3).ToLower().Trim().PadLeft(4,'0'),
                    partId = GetCell(sheet, k, 4).ToLower().Trim().PadLeft(4, '0'),
                    type = type,
                    rowNo = k,
                };
                if (typesOrderRequired.Contains(type))
                    f.order = GetCell(sheet, k, 5).ToLower().Trim();
                else if (typesBirthRequired.Contains(type))
                        f.birthOrder = GetCell(sheet, k, 5).ToLower().Trim();
                sheetRecords.Add(f);
            }
            WriteToLog("Number of data rows found:" + sheetRecords.Count);
        }

        private void GetFolderFiles(string[] dirs, string center, string hospital, ref List<FileClass> folderRecords)
        {
            string currFileName = string.Empty;
            try
            {
                foreach (string dir in dirs)
                {
                    string[] dirNameParts = Path.GetFileName(dir).Split(splitters);
                    string[] filePaths;

                    if (
                            ((!string.IsNullOrEmpty(center) && dirNameParts[1].ToLower() == center.PadLeft(4, '0').ToLower().Trim()) || string.IsNullOrEmpty(center)) &&
                            ((!string.IsNullOrEmpty(hospital) && dirNameParts[2].ToLower() == hospital) || string.IsNullOrEmpty(hospital))
                            //(dirNameParts[1].ToLower() == center.PadLeft(4, '0').ToLower().Trim()) && (dirNameParts[2].ToLower() == hospital)))
                        )
                    {
                        WriteToLog("Checking folder: " + dir);
                        filePaths = Directory.GetFiles(dir);
                        foreach (string fPath in filePaths)
                        {
                            currFileName = fPath;
                            if (allowedFileExtensions.Contains(Path.GetExtension(fPath)))
                            {
                                //if (Path.GetFileNameWithoutExtension(fPath).TrimEnd().EndsWith("C"))
                                //    continue;
                                string[] fPathParts = Path.GetFileNameWithoutExtension(fPath).Split(splitters);
                                if (fPathParts.Length < 5)
                                    continue;

                                FileClass f = new FileClass()
                                {
                                    project = fPathParts[0].Trim(),
                                    center = fPathParts[1].Trim(),
                                    hospital = fPathParts[2].Trim(),
                                    scrNo = fPathParts[3].Trim(),
                                    length = fPathParts.Length,
                                    fileName = Path.GetFileNameWithoutExtension(fPath),
                                    fileExt = Path.GetExtension(fPath).ToLower()
                                };

                                for (int i = 4; i < f.length; i++)
                                {
                                    if (allFormTypes.Contains(fPathParts[i].Trim()))
                                    {
                                        f.type = fPathParts[i].Trim();
                                        f.typePos = i + 1;
                                        break;
                                    }
                                }
                                switch (f.typePos)
                                {
                                    //case -1: //filename incorrect: type missing
                                    //    break;
                                    case 5: //the type must be SCR
                                        if (f.length == 6)
                                            f.order = fPathParts[5].Trim();
                                        break;
                                    case 6:
                                        f.partId = fPathParts[4].Trim();
                                        if (f.length == 7)
                                            if (typesOrderRequired.Contains(f.type))
                                                f.order = fPathParts[6].Trim();
                                        break;
                                    case 7:
                                        f.partId = fPathParts[4].Trim();
                                        if (f.length == 7)
                                            if (typesBirthRequired.Contains(f.type))
                                                f.birthOrder = fPathParts[5].Trim();
                                        break;
                                    default:
                                        break;
                                }
                                folderRecords.Add(f);
                            }
                            else
                            {
                                FileClass f = new FileClass()
                                {
                                    fileName = Path.GetFileName(fPath),
                                    fileExt = string.Empty
                                };
                                folderRecords.Add(f);
                            }
                        }
                        WriteToLog("Number of files found:" + folderRecords.Count);
                    }
                }
            }
            catch (Exception ex)
            {

                // Get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                MessageBox.Show("Line#" + line + ";Error:" + ex.Message+ "; FileName="+currFileName);
            }
        }

        public void CheckExcelRecords(ref List<FileClass> sheetRecords, ref List<FileClass> folderRecords, string project)
        {   
            foreach (FileClass sheetRecord in sheetRecords)
            {
                foreach (FileClass fileRecord in folderRecords.FindAll(file => !file.processed))
                {
                    if (typesOrderRequired.Contains(sheetRecord.type))
                        if (String.IsNullOrEmpty(sheetRecord.order))
                            continue;

                    if (typesBirthRequired.Contains(sheetRecord.type))
                        if (String.IsNullOrEmpty(sheetRecord.birthOrder))
                            continue;

                    if ((sheetRecord.center.PadLeft(4, '0') == fileRecord.center) &&
                        (sheetRecord.hospital.PadLeft(2, '0') == fileRecord.hospital.PadLeft(2, '0')) &&
                        (sheetRecord.scrNo.PadLeft(4, '0') == fileRecord.scrNo) &&
                        (sheetRecord.partId.PadLeft(4, '0') == fileRecord.partId.PadLeft(4, '0')) 
                        )
                    {

                        if (typesOrderRequired.Contains(sheetRecord.type)) //form with order
                        {
                            if (sheetRecord.order.Trim() == "1")
                            {
                                if (sheetRecords.Exists(r => ((r.scrNo == sheetRecord.scrNo) && (r.order != sheetRecord.order))))
                                {
                                    if (sheetRecord.order == fileRecord.order)
                                    {
                                        sheetRecord.found = true;
                                        fileRecord.processed = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    //if (!folderRecords.Exists(r => ((r.scrNo == sheetRecord.scrNo) && (r.order != sheetRecord.order))))
                                    if (folderRecords.Exists(r => r.scrNo == sheetRecord.scrNo && r.order == "1"))
                                        break;
                                    else
                                    {
                                        sheetRecord.found = true;
                                        fileRecord.processed = true;
                                        break;
                                    }

                                }

                            }
                            else if(sheetRecord.order.Trim()!="1")
                            {
                                if (sheetRecord.order == fileRecord.order)
                                {
                                    sheetRecord.found = true;
                                    fileRecord.processed = true;
                                    break;
                                }
                            }
                            else
                            {

                                sheetRecord.found = true;
                                fileRecord.processed = true;
                                break;
                            }
                        }

                        if (typesBirthRequired.Contains(sheetRecord.type)) //form with birth order
                        {
                            if (sheetRecord.birthOrder.Trim() == "1")
                            {
                                if (sheetRecords.Exists(r => ((r.scrNo == sheetRecord.scrNo) && (r.birthOrder != sheetRecord.birthOrder))))
                                {
                                    if (sheetRecord.birthOrder == fileRecord.birthOrder)
                                    {
                                        sheetRecord.found = true;
                                        fileRecord.processed = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    //if (!folderRecords.Exists(r => ((r.scrNo == sheetRecord.scrNo) && (r.birthOrder != sheetRecord.birthOrder))))
                                    if(folderRecords.Exists(r=>r.scrNo==sheetRecord.scrNo && r.birthOrder=="1"))
                                        break;
                                    else
                                    {
                                        sheetRecord.found = true;
                                        fileRecord.processed = true;
                                        break;
                                    }
                                }
                            }
                            else if (sheetRecord.birthOrder.Trim() != "1")
                            {
                                if (sheetRecord.birthOrder == fileRecord.birthOrder)
                                {
                                    sheetRecord.found = true;
                                    fileRecord.processed = true;
                                    break;
                                }
                            }
                            else
                            {
                                sheetRecord.found = true;
                                fileRecord.processed = true;
                                break;
                            }
                        }

                        if (typesOrderAndBirthOrderNotRequired.Contains(sheetRecord.type))  //simple form
                        {
                            sheetRecord.found = true;
                            fileRecord.processed = true;
                            break;
                        }
                    }
                }
            }
        }


        public void ValidateFileNames(ref List<FileClass> folderRecords, ref List<FileClass> sheetRecords, string project, string folderCenter, string folderHospital)
        {
            foreach (FileClass fileRecord in folderRecords)
            {
                if (!allFormTypes.Contains(fileRecord.type))
                {
                    fileRecord.error = true;
                    fileRecord.errorMessage = "Incorrect filename: filetype";
                    continue;
                }
                if (fileRecord.length>7)
                {
                    fileRecord.error = true;
                    fileRecord.errorMessage = "Incorrect filename: too many \"-\"";
                    continue;
                }

                if (!allowedFileExtensions.Contains(fileRecord.fileExt))
                {
                    fileRecord.error = true;
                    fileRecord.errorMessage = "Incorrect filename: file extension";
                    continue;
                }

                if (fileRecord.project != project)
                {
                    fileRecord.error = true;
                    fileRecord.errorMessage = "Incorrect filename: project name";
                    continue;
                }

                if (fileRecord.length == 5)
                {
                    if (fileRecord.type != "SCR")
                    {
                        fileRecord.error = true;
                        fileRecord.errorMessage = "Incorrect filename: missing \"-\"";
                        continue;
                    }
                }

                if (typesOrderRequired.Contains(fileRecord.type))
                {
                    if (String.IsNullOrEmpty(fileRecord.order))
                    {
                        if (sheetRecords.FindAll(r => r.scrNo == fileRecord.scrNo).Count > 1)
                        {
                            fileRecord.error = true;
                            fileRecord.errorMessage = "Incorrect filename: missing order";
                            continue;
                        }
                    }
                    else
                    {
                        if (sheetRecords.FindAll(r => r.scrNo == fileRecord.scrNo).Count == 1)
                        {
                            fileRecord.error = true;
                            fileRecord.errorMessage = "Incorrect filename: order not required";
                            continue;
                        }

                    }
                }


                if (typesBirthRequired.Contains(fileRecord.type))
                {
                    if (String.IsNullOrEmpty(fileRecord.birthOrder))
                    {
                        if (sheetRecords.FindAll(r => r.scrNo == fileRecord.scrNo).Count > 1)
                        {
                            fileRecord.error = true;
                            fileRecord.errorMessage = "Incorrect filename: missing birth order";
                            continue;
                        }
                    }
                    else
                    {
                        if (sheetRecords.FindAll(r => r.scrNo == fileRecord.scrNo).Count == 1)
                        {
                            fileRecord.error = true;
                            fileRecord.errorMessage = "Incorrect filename: birth order not required";
                            continue;
                        }
                    }
                }


                if (fileRecord.length<4)
                {
                    fileRecord.error = true;
                    fileRecord.errorMessage = "Incorrect filename: missing \"-\"";
                    continue;
                }


                if (fileRecord.center != folderCenter)
                {
                    fileRecord.error = true;
                    fileRecord.errorMessage = "Incorrect filename: center";
                    continue;
                }

                if (fileRecord.hospital.PadLeft(2, '0') != folderHospital.PadLeft(2, '0'))
                {
                    fileRecord.error = true;
                    fileRecord.errorMessage = "Incorrect filename: hospital";
                    continue;
                }
            }

            WriteToLog("ERRORS:");
            foreach (FileClass fileRecord in folderRecords.FindAll(r=>r.error))
                WriteToLog(fileRecord.fileName + ": " + fileRecord.errorMessage);
        }

        public void CheckFolderRecords(ref List<FileClass> folderRecords, ref List<FileClass> sheetRecords, string project)
        {
            foreach (FileClass fileRecord in folderRecords.FindAll(file => !file.error))
            {
                if (fileRecord.processed)
                    fileRecord.found = true;
                else
                {
                    fileRecord.found = false;
                    if(String.IsNullOrEmpty(fileRecord.errorMessage))
                        fileRecord.errorMessage = "File not found in Excel";
                }
            }
            WriteToLog("Not found in EXCEL:");
            foreach (FileClass fileRecord in folderRecords.FindAll(r => (!r.found&&!r.error)))
                WriteToLog(fileRecord.fileName + ": " + fileRecord.errorMessage);
        }

        public void CheckScrForOrderIssue(ref List<FileClass> folderRecords)
        {
            foreach (FileClass fileRecord in folderRecords.FindAll(file => file.type == "SCR" && file.order == "2" && string.IsNullOrEmpty(file.partId)))
            {
                if (folderRecords.FindAll(f => f.order == "1" && f.scrNo == fileRecord.scrNo && !string.IsNullOrEmpty(f.partId)).Count == 1)
                {
                    fileRecord.error = true;
                    fileRecord.errorMessage = "Error #1";
                }
            }

            foreach (FileClass fileRecord in folderRecords.FindAll(file => !file.error && file.type == "SCR" && file.order == "3" && !string.IsNullOrEmpty(file.partId)))
            {
                if (folderRecords.FindAll(f => f.order == "2" && f.scrNo == fileRecord.scrNo && string.IsNullOrEmpty(f.partId)).Count == 1)
                {
                    if (folderRecords.FindAll(f => f.order == "1" && f.scrNo == fileRecord.scrNo && string.IsNullOrEmpty(f.partId)).Count == 1)
                    {
                        fileRecord.error = true;
                        fileRecord.errorMessage = "Error #2";
                    }
                }
            }

            WriteToLog("Errors SCR SOP:");
            foreach (FileClass fileRecord in folderRecords.FindAll(r => r.error))
                WriteToLog(fileRecord.fileName + ": " + fileRecord.errorMessage);
        }
        
        private void startBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string currentFilepath = string.Empty;
                string currentSheetName = string.Empty;

                Excel.Application xlApp = new Excel.Application();
                xlApp.DisplayAlerts = false;
                Excel.Workbooks xlWorkBooks = xlApp.Workbooks;

                try
                {
                    WriteToLog("************************************************************************************************");
                    WriteToLog("Started at:" + System.DateTime.Now);
                    xlApp.AutoCorrect.AutoFillFormulasInLists = false;
                    if (String.IsNullOrEmpty(dbFolder))
                    {
                        MessageBox.Show("Select folder for database!");
                        return;
                    }
                    if (String.IsNullOrEmpty(fileFolder))
                    {
                        MessageBox.Show("Select folder for files!");
                        return;
                    }
                    if (String.IsNullOrEmpty(projectNameTxtBox.Text))
                    {
                        MessageBox.Show("Set the project id!");
                        return;
                    }
                    string[] files, dirs;
                    string fName;
                    string type;
                    string center3 = string.Empty, hospital3 = string.Empty, scrNo = string.Empty, partId = string.Empty, order = string.Empty;

                    if ((bool)checkSCR.IsChecked)
                    {
                        List<FileClass> folderAllRecords = new List<FileClass>();
                        GetAllFolders(fileFolder, "SCR");
                        GetFolderFiles(dirsList.ToArray(), string.Empty, string.Empty, ref folderAllRecords);
                        CheckScrForOrderIssue(ref folderAllRecords);
                    }

                    files = Directory.GetFiles(dbFolder, "*.xls*", SearchOption.TopDirectoryOnly);



                    foreach (string filepath in files)  //process each database file(XLS) in the folder
                    {
                        dirsList.Clear();
                        Dictionary<string, string> errors = new Dictionary<string, string>();
                        List<string> lType5 = new List<string>();

                        if (filepath.Contains("~")) { continue; } //ignore currently open temporary file

                        currentFilepath = filepath;

                        fName = Path.GetFileName(filepath);


                        //Database fileType is retrieved from filename, the first element after splitting
                        type = fName.Split(splitters)[0];

                        /*LOG Start*/
                        WriteToLog("-------------------------------------------------------------------------------------------");
                        WriteToLog("Processing file:" + filepath);
                        WriteToLog("Filetype:" + type);
                        /*LOG End*/

                        //Get all dropbox/file folders that match with type into dirList
                        GetAllFolders(fileFolder, type);
                        dirs = dirsList.ToArray();

                        /*LOG Start*/
                        if (!LogFoundDirs(dirs, type))
                            continue;
                        /*LOG End*/

                        /*Open the XLS workbook*/
                        Excel.Workbook xlWorkBook = xlWorkBooks.Open(filepath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                                        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                        /*Process each worksheet in the XLS workbook*/
                        foreach (Excel.Worksheet sheet in xlWorkBook.Worksheets)
                        {
                            /*Search result must be recorder in column name="dropbox" that can be in different positions*/
                            int dropBoxColumnNo = 0;
                            int processedRowsCount = startingRow - 1;
                            List<FileClass> sheetRecords = new List<FileClass>();
                            List<FileClass> folderRecords = new List<FileClass>();
                            string center = string.Empty, hospital = string.Empty;

                            currentSheetName = sheet.Name;



                            int dataRowsCount = sheet.Cells.Find("*", System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, Excel.XlSearchOrder.xlByRows, Excel.XlSearchDirection.xlPrevious,
                                                                    false, System.Reflection.Missing.Value, System.Reflection.Missing.Value).Row;
                            int dataColumnsCount = sheet.Cells.Find("*", System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, Excel.XlSearchOrder.xlByRows, Excel.XlSearchDirection.xlPrevious,
                                                                    false, System.Reflection.Missing.Value, System.Reflection.Missing.Value).Column;

                            WriteToLog("Processing sheetname:" + sheet.Name + "(Rows=" + dataRowsCount + " ,Columns=" + dataColumnsCount + ")");

                            /*Requirements to XLS format: 1-center, 2-hospital, 3-scr number, 4-participant*/
                            if (!isFormatCorrect(sheet, startingRow - 1, type, ref dropBoxColumnNo)) continue;

                            Excel.Range c1, c2;
                            c1 = sheet.Cells[startingRow - 3, dropBoxColumnNo - 1];
                            c2 = sheet.Cells[startingRow - 3, dropBoxColumnNo];
                            sheet.Range[c1, c2].Merge();
                            c1.Value2 = "C = Completed";
                            c1 = sheet.Cells[startingRow - 2, dropBoxColumnNo - 1];
                            c2 = sheet.Cells[startingRow - 2, dropBoxColumnNo];
                            sheet.Range[c1, c2].Merge();
                            c1.Value2 = "File missing in Dropbox";
                            c1.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.PeachPuff);

                            GetExcelRows(sheet, startingRow, dataRowsCount, type, ref sheetRecords, ref center, ref hospital);

                            GetFolderFiles(dirs, center, hospital, ref folderRecords);
                            ValidateFileNames(ref folderRecords, ref sheetRecords, projectNameTxtBox.Text, center, hospital);

                            CheckExcelRecords(ref sheetRecords, ref folderRecords, projectNameTxtBox.Text);

                            CheckFolderRecords(ref folderRecords, ref sheetRecords, projectNameTxtBox.Text);

                            Excel.Range cell;
                            foreach (FileClass sheetRecord in sheetRecords)
                            {
                                if (sheetRecord.found)
                                {
                                    cell = sheet.Cells[sheetRecord.rowNo, dropBoxColumnNo];
                                    cell.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                                    cell.Value2 = "C";
                                    cell.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                    //WriteToLog(Path.GetFileName(fPath) + ":OK");
                                }
                                else
                                {
                                    if (GetCell(sheet, sheetRecord.rowNo, 1) != string.Empty)
                                    {
                                        cell = sheet.Cells[sheetRecord.rowNo, dropBoxColumnNo];
                                        cell.Value2 = string.Empty;
                                        cell.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.PeachPuff);
                                    }
                                }
                                processedRowsCount++;
                            }
                            cell = sheet.Cells[processedRowsCount + 1, dropBoxColumnNo];
                            cell.Value2 = sheetRecords.FindAll(r => r.found).Count.ToString();
                            cell.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                        }
                        xlWorkBook.SaveAs(filepath, Excel.XlFileFormat.xlOpenXMLWorkbook, Type.Missing, Type.Missing, false, false, Excel.XlSaveAsAccessMode.xlNoChange,
                                            Excel.XlSaveConflictResolution.xlLocalSessionChanges, true,
                                            Type.Missing, Type.Missing, Type.Missing);

                        xlWorkBook.Close();
                        Marshal.ReleaseComObject(xlWorkBook);
                    }
                    WriteToLog("-------------------------------------------------------------------------------------------");
                    WriteToLog("Completed at:" + System.DateTime.Now);
                }
                catch (Exception ex)
                {
                    WriteToLog("-------------------------------------------------------------------------------------------");
                    WriteToLog("Completed at:" + System.DateTime.Now);
                    MessageBox.Show("FileName:" + currentFilepath + " | SheetName = " + currentSheetName + " | Exception:" + ex.ToString());
                }
                finally
                {
                    xlWorkBooks.Close();
                    xlApp.Quit();
                    Marshal.ReleaseComObject(xlWorkBooks);
                    Marshal.ReleaseComObject(xlApp);
                    MessageBox.Show("Data processing finished. For Details please refer to log.");
                }
            }
            catch (Exception ex)
            {
                // Get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                MessageBox.Show("Line#" + line + ";Error:" + ex.Message);
            }
        }

        public static string GetCell(Excel._Worksheet worksheet, int row, int column)
        {
            Excel.Range range = worksheet.Cells[row, column] as Excel.Range;
            if (range == null || range.Value2 == null)
            {
                return string.Empty;
            }
            return range.Value2.ToString();
        }

        private void WriteToLog(string logString)
        {
            string logpath;
            if (Directory.Exists(path))
                logpath = path + "log.txt";
            else logpath = AppDomain.CurrentDomain.BaseDirectory + "log.txt";
            if (!File.Exists(logpath))
            {
                File.Create(logpath).Dispose();
                using (var tw = new System.IO.StreamWriter(logpath))
                {
                    tw.WriteLine(logString);
                    tw.Close();
                }
            }
            else if (File.Exists(logpath))
            {
                using (var tw = new System.IO.StreamWriter(logpath, true))
                {
                    tw.WriteLine(logString);
                    tw.Close();
                }
            }
        }

        private void dbFolderPathBtn_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                string path = @"C:\My files\WHO_SIS\FileNameSearchEngine\Db\";

                if (Directory.Exists(path))
                    dialog.SelectedPath = path;
                else
                    dialog.SelectedPath = @"C:\";
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    dbFolderPath.Text = dialog.SelectedPath;
                    dbFolder = dbFolderPath.Text;
                }
            }
        }
    }
}
